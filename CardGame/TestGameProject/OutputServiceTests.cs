﻿using CardGame.Interfaces;
using CardGame.Services;
using NUnit.Framework;

namespace TestGameProject
{
    public class OutputServiceTests
    {
        private readonly IOutputService _outputService;
        public OutputServiceTests()
        {
            _outputService = new OutputService();
        }

        [Test]
        public void Log_ValidData_ShouldNotBeException()
        {
            _outputService.Log("Hello");
        }
    }
}
