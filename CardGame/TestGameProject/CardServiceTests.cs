﻿using CardGame;
using CardGame.Interfaces;
using CardGame.Services;
using NUnit.Framework;
using System.Collections.Generic;

namespace TestGameProject
{
    public class CardServiceTests
    {
        private readonly ICardService _cardService;
        public CardServiceTests()
        {
            _cardService = new CardService();
        }

        [TestCase(40)]
        [TestCase(4)]
        public void GetCards_ValidData_ShouldReturnValidResponse(int countCards)
        {
            // Arrange
            var extendResult = countCards == 40 ? GetExtendetResultForFourtyCards() : GetExtendetResultForFourCards();

            // Act
            var result = _cardService.GetCards(countCards);

            // Assert
            if (countCards == 40)
            {
                Assert.AreEqual(40, result.Count);
            }
            else
            {
                Assert.AreEqual(4, result.Count);
            }

            for (var i = 0; i < extendResult.Count; i++)
            {
                Assert.AreEqual(extendResult[i].Name, result[i].Name);
                Assert.AreEqual(extendResult[i].Suit, result[i].Suit);
                Assert.AreEqual(extendResult[i].Number, result[i].Number);
            }
        }

        private List<Card> GetExtendetResultForFourCards()
        {
            return new List<Card>
            {
                new Card { Name = "1", Number = 1, Suit = "Heart" },
                new Card { Name = "1", Number = 1, Suit = "Diamond" },
                new Card { Name = "1", Number = 1, Suit = "Club" },
                new Card { Name = "1", Number = 1, Suit = "Spade" },
            };
        }

        [Test]
        public void ShuffleCards_ValidData_ShouldReturnValidResponse()
        {
            // Arrange
            var extendResult = GetExtendetResultForFourtyCards();

            // Act
            var result = _cardService.ShuffleCards(GetExtendetResultForFourtyCards());

            // Assert
            Assert.AreEqual(extendResult.Count, result.Count);
        }

        [Test]
        public void ShuffleCards_ValidData_ShouldNull()
        {
            // Act
            var result = _cardService.ShuffleCards(new List<Card>());

            // Assert
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void DealCards_ValidData_ShouldReturnValidResponse()
        {
            // Arrange
            var extendData = GetExtendDataForDealCards();

            // Act
            var result = _cardService.DealCards(GetDataForShuffleCards(), 8);

            // Assert
            Assert.AreEqual(result[0].Cards.Count, result[1].Cards.Count);
            for (var i = 0; i < result.Count; i++)
            {
                Assert.AreEqual(extendData.Count, result.Count);
                Assert.AreEqual(extendData[i].Cards.Count, result[i].Cards.Count);
                Assert.AreEqual(extendData[i].Name, result[i].Name);
            }
        }

        #region PrivateMethods

        private List<Card> GetExtendetResultForFourtyCards()
        {
            return new List<Card>
            {
                new Card{Name = "1", Number = 1, Suit = "Heart"},
            new Card{Name = "1", Number = 1, Suit = "Diamond"},
            new Card{Name = "1", Number = 1, Suit = "Club"},
            new Card{Name = "1", Number = 1, Suit = "Spade"},

            new Card{Name = "2", Number = 2, Suit = "Heart"},
            new Card{Name = "2", Number = 2, Suit = "Diamond"},
            new Card{Name = "2", Number = 2, Suit = "Club"},
            new Card{Name = "2", Number = 2, Suit = "Spade"},

            new Card{Name = "3", Number = 3, Suit = "Heart"},
            new Card{Name = "3", Number = 3, Suit = "Diamond"},
            new Card{Name = "3", Number = 3, Suit = "Club"},
            new Card{Name = "3", Number = 3, Suit = "Spade"},

            new Card{Name = "4", Number = 4, Suit = "Heart"},
            new Card{Name = "4", Number = 4, Suit = "Diamond"},
            new Card{Name = "4", Number = 4, Suit = "Club"},
            new Card{Name = "4", Number = 4, Suit = "Spade"},

            new Card{Name = "5", Number = 5, Suit = "Heart"},
            new Card{Name = "5", Number = 5, Suit = "Diamond"},
            new Card{Name = "5", Number = 5, Suit = "Club"},
            new Card{Name = "5", Number = 5, Suit = "Spade"},

            new Card{Name = "6", Number = 6, Suit = "Heart"},
            new Card{Name = "6", Number = 6, Suit = "Diamond"},
            new Card{Name = "6", Number = 6, Suit = "Club"},
            new Card{Name = "6", Number = 6, Suit = "Spade"},

            new Card{Name = "7", Number = 7, Suit = "Heart"},
            new Card{Name = "7", Number = 7, Suit = "Diamond"},
            new Card{Name = "7", Number = 7, Suit = "Club"},
            new Card{Name = "7", Number = 7, Suit = "Spade"},

            new Card{Name = "8", Number = 8, Suit = "Heart"},
            new Card{Name = "8", Number = 8, Suit = "Diamond"},
            new Card{Name = "8", Number = 8, Suit = "Club"},
            new Card{Name = "8", Number = 8, Suit = "Spade"},

            new Card{Name = "9", Number = 9, Suit = "Heart"},
            new Card{Name = "9", Number = 9, Suit = "Diamond"},
            new Card{Name = "9", Number = 9, Suit = "Club"},
            new Card{Name = "9", Number = 9, Suit = "Spade"},

             new Card{Name = "10", Number = 10, Suit = "Heart"},
             new Card{Name = "10", Number = 10, Suit = "Diamond"},
             new Card{Name = "10", Number = 10, Suit = "Club"},
             new Card{Name = "10", Number = 10, Suit = "Spade"},
            };
        }

        private List<Player> GetDataForShuffleCards()
        {
            return new List<Player>
            {
                new Player{Name = "Player1",},
                new Player{Name = "Player2",},
            };
        }

        private List<Player> GetExtendDataForDealCards()
        {
            return new List<Player>
            {
                new Player
                {
                    Name = "Player1",
                    Cards = new List<Card>
                    {
                        new Card{Name = "1", Number = 1, Suit = "Heart"},
                        new Card{Name = "1", Number = 1, Suit = "Club"},

                        new Card{Name = "2", Number = 2, Suit = "Heart"},
                        new Card{Name = "2", Number = 2, Suit = "Club"},
                    },
                },
                new Player
                {
                    Name = "Player2",
                    Cards = new List<Card>
                    {
                        new Card{Name = "1", Number = 1, Suit = "Diamond"},
                        new Card{Name = "1", Number = 1, Suit = "Spade"},

                        new Card{Name = "2", Number = 2, Suit = "Diamond"},
                        new Card{Name = "2", Number = 2, Suit = "Spade"},
                    },
                },
            };
        }

        #endregion
    }
}
