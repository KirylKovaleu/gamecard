﻿using CardGame;
using CardGame.Implementation;
using CardGame.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace TestGameProject
{
    public class GameServiceTests
    {
        private readonly Mock<ICardService> _cardService;
        private readonly Mock<IPlayerService> _playerService;
        private readonly Mock<IOutputService> _outputService;
        private readonly IGameService _gameService;
        public GameServiceTests()
        {
            _cardService = new Mock<ICardService>();
            _playerService = new Mock<IPlayerService>();
            _outputService = new Mock<IOutputService>();
            _gameService = new GameService(_cardService.Object, _playerService.Object, _outputService.Object);
        }

        [Test]
        public void RunGame_ValidData_ShouldReturnPlayer1Win()
        {
            // Arrange
            var extend = GetExtendDataForRunGame();
            var res = _cardService.Setup(x => x.ShuffleCards(It.IsAny<List<Card>>()))
                .Returns(new Queue<List<Card>>(new[]
                {
                    new List<Card>{
                        new Card{Name = "2", Number = 2, Suit = "Heart"},
                        new Card{Name = "2", Number = 2, Suit = "Club"},
                        new Card{Name = "1", Number = 1, Suit = "Diamond"},
                        new Card{Name = "1", Number = 1, Suit = "Spade"},
                    },
                    new List<Card>()
                }).Dequeue);

            // Act
            var testData = GetTestDataForRunGame();
            var result = _gameService.RunGame(testData[0], testData[1]);

            // Assert
            Assert.AreEqual(4, result.Cards.Count);
            Assert.AreEqual(0, result.DiscardDeck.Count);
            Assert.AreEqual("Player1", result.Name);
        }

        [Test]
        public void RunGame_ValidData_ShouldStartShuffleCardsForDiscardDeck()
        {
            // Arrange
            var discardDeck = new List<Card>();

            _cardService.Setup(x => x.ShuffleCards(It.IsAny<List<Card>>()))
                .Callback<List<Card>>(x =>
                {
                    discardDeck = x;
                });

            // Act
            var testData = GetTestDataForShuffleCards();
            var result = _gameService.RunGame(testData[0], testData[1]);

            // Assert
            Assert.AreEqual(5, discardDeck.Count);
            Assert.AreEqual(null, result);
        }

        #region PrivateMethods

        private List<Card> GetExtendDataForRunGame()
        {
            return new List<Card>
            {
                new Card{Name = "2", Number = 2, Suit = "Heart"},
                new Card{Name = "2", Number = 2, Suit = "Club"},
                new Card{Name = "1", Number = 1, Suit = "Diamond"},
                new Card{Name = "1", Number = 1, Suit = "Spade"},
            };
        }

        private List<Player> GetTestDataForRunGame()
        {
            return new List<Player>
            {
                new Player
                {
                    Name = "Player1",
                    Cards = new List<Card>
                    {
                        new Card{Name = "2", Number = 2, Suit = "Heart"},
                        new Card{Name = "2", Number = 2, Suit = "Club"},
                    },
                },
                new Player
                {
                    Name = "Player2",
                    Cards = new List<Card>
                    {
                        new Card{Name = "1", Number = 1, Suit = "Diamond"},
                        new Card{Name = "1", Number = 1, Suit = "Spade"},
                    },
                },
            };
        }

        private List<Player> GetTestDataForShuffleCards()
        {
            return new List<Player>
            {
                new Player
                {
                    Name = "Player1",
                    Cards = new List<Card>
                    {
                        new Card{Name = "2", Number = 2, Suit = "Heart"},
                    },
                    DiscardDeck = new List<Card>
                    {
                        new Card{Name = "2", Number = 2, Suit = "Heart"},
                        new Card{Name = "4", Number = 2, Suit = "Heart"},
                        new Card{Name = "5", Number = 2, Suit = "Heart"},
                    },
                },
                new Player
                {
                    Name = "Player2",
                    Cards = new List<Card>
                    {
                        new Card{Name = "1", Number = 1, Suit = "Diamond"},
                        new Card{Name = "1", Number = 1, Suit = "Spade"},
                    },
                },
            };
        }

        #endregion
    }
}
