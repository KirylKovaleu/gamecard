﻿using CardGame;
using CardGame.Interfaces;
using CardGame.Services;
using NUnit.Framework;
using System.Collections.Generic;

namespace TestGameProject
{
    public class PlayerServiceTests
    {
        private readonly IPlayerService _playerService;

        public PlayerServiceTests()
        {
            _playerService = new PlayerService();
        }

        [Test]
        public void Log_ValidData_ShouldReturnValidResponse()
        {
            // Arrange
            var expected = GetExpectedData();
            // Act
            var result = _playerService.CreatePayers(2);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(expected.Count, result.Count);

            for(var i = 0; i< expected.Count; i++)
            {
                Assert.AreEqual(expected[i].Name, result[i].Name);
            }
        }

        #region PrivateMethods

        private List<Player> GetExpectedData()
        {
            return new List<Player>
            {
                new Player
                {
                    Name = "Player1",
                },
                new Player
                {
                    Name = "Player2",
                },
            };
        }

        #endregion
    }
}
