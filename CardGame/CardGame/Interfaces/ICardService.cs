﻿using System.Collections.Generic;

namespace CardGame.Interfaces
{
    public interface ICardService
    {
        List<Card> GetCards(int countOfCards);

        List<Card> ShuffleCards(List<Card> cards);

        List<Player> DealCards(List<Player> players, int countOfCards);
    }
}
