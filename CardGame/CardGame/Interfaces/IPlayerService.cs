﻿using System.Collections.Generic;

namespace CardGame.Interfaces
{
    public interface IPlayerService
    {
        List<Player> CreatePayers(int countOfPlayer);
    }
}
