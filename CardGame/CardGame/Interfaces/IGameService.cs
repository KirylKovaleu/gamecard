﻿using System.Collections.Generic;

namespace CardGame.Interfaces
{
    public interface IGameService
    {
        void StartGame();

        Player RunGame(Player playerOne, Player playerTwo);
    }
}
