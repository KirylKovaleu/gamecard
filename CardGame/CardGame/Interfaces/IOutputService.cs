﻿namespace CardGame.Interfaces
{
    public interface IOutputService
    {
        void Log<T>(string message, T info);

        void Log(string message);
    }
}
