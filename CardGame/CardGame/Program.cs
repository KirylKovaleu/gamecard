﻿using CardGame.Implementation;
using CardGame.Interfaces;
using CardGame.Services;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CardGame
{
    class Program
    {
        static void Main(string[] args)
        {
            var game = GetService<IGameService>();
            game.StartGame();

            Console.Read();
        }

        public static T GetService<T>()
        {
            var serviceProvider = new ServiceCollection()
            .AddTransient<IGameService, GameService>()
            .AddTransient<ICardService, CardService>()
            .AddTransient<IPlayerService, PlayerService>()
            .AddTransient<IOutputService, OutputService>()
            .BuildServiceProvider();

            return serviceProvider.GetService<T>();
        }
    }
}
