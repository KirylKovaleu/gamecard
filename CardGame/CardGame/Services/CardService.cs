﻿using CardGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CardGame.Services
{
    public class CardService : ICardService
    {
        private List<Card> cards = new List<Card>
        {
            new Card{Name = "1", Number = 1, Suit = "Heart"},
            new Card{Name = "1", Number = 1, Suit = "Diamond"},
            new Card{Name = "1", Number = 1, Suit = "Club"},
            new Card{Name = "1", Number = 1, Suit = "Spade"},

            new Card{Name = "2", Number = 2, Suit = "Heart"},
            new Card{Name = "2", Number = 2, Suit = "Diamond"},
            new Card{Name = "2", Number = 2, Suit = "Club"},
            new Card{Name = "2", Number = 2, Suit = "Spade"},

            new Card{Name = "3", Number = 3, Suit = "Heart"},
            new Card{Name = "3", Number = 3, Suit = "Diamond"},
            new Card{Name = "3", Number = 3, Suit = "Club"},
            new Card{Name = "3", Number = 3, Suit = "Spade"},

            new Card{Name = "4", Number = 4, Suit = "Heart"},
            new Card{Name = "4", Number = 4, Suit = "Diamond"},
            new Card{Name = "4", Number = 4, Suit = "Club"},
            new Card{Name = "4", Number = 4, Suit = "Spade"},

            new Card{Name = "5", Number = 5, Suit = "Heart"},
            new Card{Name = "5", Number = 5, Suit = "Diamond"},
            new Card{Name = "5", Number = 5, Suit = "Club"},
            new Card{Name = "5", Number = 5, Suit = "Spade"},

            new Card{Name = "6", Number = 6, Suit = "Heart"},
            new Card{Name = "6", Number = 6, Suit = "Diamond"},
            new Card{Name = "6", Number = 6, Suit = "Club"},
            new Card{Name = "6", Number = 6, Suit = "Spade"},

            new Card{Name = "7", Number = 7, Suit = "Heart"},
            new Card{Name = "7", Number = 7, Suit = "Diamond"},
            new Card{Name = "7", Number = 7, Suit = "Club"},
            new Card{Name = "7", Number = 7, Suit = "Spade"},

            new Card{Name = "8", Number = 8, Suit = "Heart"},
            new Card{Name = "8", Number = 8, Suit = "Diamond"},
            new Card{Name = "8", Number = 8, Suit = "Club"},
            new Card{Name = "8", Number = 8, Suit = "Spade"},

            new Card{Name = "9", Number = 9, Suit = "Heart"},
            new Card{Name = "9", Number = 9, Suit = "Diamond"},
            new Card{Name = "9", Number = 9, Suit = "Club"},
            new Card{Name = "9", Number = 9, Suit = "Spade"},

             new Card{Name = "10", Number = 10, Suit = "Heart"},
             new Card{Name = "10", Number = 10, Suit = "Diamond"},
             new Card{Name = "10", Number = 10, Suit = "Club"},
             new Card{Name = "10", Number = 10, Suit = "Spade"},

             new Card{Name = "Jack", Number = 11, Suit = "Heart"},
             new Card{Name = "Jack", Number = 11, Suit = "Diamond"},
             new Card{Name = "Jack", Number = 11, Suit = "Club"},
             new Card{Name = "Jack", Number = 11, Suit = "Spade"},

             new Card{Name = "Queen", Number = 12, Suit = "Heart"},
             new Card{Name = "Queen", Number = 12, Suit = "Diamond"},
             new Card{Name = "Queen", Number = 12, Suit = "Club"},
             new Card{Name = "Queen", Number = 12, Suit = "Spade"},

             new Card{Name = "King", Number = 13, Suit = "Heart"},
             new Card{Name = "King", Number = 13, Suit = "Diamond"},
             new Card{Name = "King", Number = 13, Suit = "Club"},
             new Card{Name = "King", Number = 13, Suit = "Spade"},

             new Card{Name = "Ace", Number = 14, Suit = "Heart"},
             new Card{Name = "Ace", Number = 14, Suit = "Diamond"},
             new Card{Name = "Ace", Number = 14, Suit = "Club"},
             new Card{Name = "Ace", Number = 14, Suit = "Spade"},
        };

        public List<Card> GetCards(int countOfCards)
        {
            return cards.GetRange(0, countOfCards);
        }

        public List<Card> ShuffleCards(List<Card> cards)
        {
            if (!cards.Any())
                return cards;

            var rnd = new Random();
            var cardCount = cards.Count;
            for (int i = 0; i < cardCount; i++)
            {
                int r = rnd.Next(0, cardCount);
                var t = cards[r];
                cards[r] = cards[i];
                cards[i] = t;
            }

            return cards;
        }

        public List<Player> DealCards(List<Player> players, int countOfCards)
        {
            var cards = GetCards(countOfCards);
            var shuffleCards = ShuffleCards(cards);

            for (var i = 0; i < shuffleCards.Count;)
            {
                players[i].Cards.Add(shuffleCards[i]);
                if (i == players.Count - 1)
                {
                    shuffleCards.RemoveRange(0, players.Count);
                    i = 0;
                }
                else
                {
                    i++;
                }
            }

            return players;
        }
    }
}
