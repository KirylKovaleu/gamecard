﻿using CardGame.Constants;
using CardGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CardGame.Implementation
{
    public class GameService : IGameService
    {
        private readonly ICardService _cardService;
        private readonly IPlayerService _playerService;
        private readonly IOutputService _outputService;

        public GameService(ICardService cardService, IPlayerService playerService, IOutputService outputService)
        {
            _cardService = cardService;
            _playerService = playerService;
            _outputService = outputService;
        }

        public void StartGame()
        {
            var isCorrect = false;
            do
            {
                _outputService.Log(OutputConstants.EnterNumber);
                int.TryParse(Console.ReadLine(), out int res);

                if (res % 4 == 0 && res <= 56 && res > 0)
                {
                    isCorrect = !isCorrect;
                    var players = _playerService.CreatePayers(2);
                    _cardService.DealCards(players, res);

                    var winner = RunGame(players[0], players[1]);

                    _outputService.Log(OutputConstants.WinPlayerInRGame, winner.Name);
                }
                else
                {
                    _outputService.Log(OutputConstants.NotCorrectNumber);
                }
            }
            while (!isCorrect);
        }

        public Player RunGame(Player playerOne, Player playerTwo)
        {
            try
            {
                var winnerCards = new List<Card>();

                while (playerOne.Cards.Any() && playerTwo.Cards.Any())
                {
                    var cardFirstPlayer = playerOne.Cards[0];
                    var cardSecondPlayer = playerTwo.Cards[0];
                    _outputService.Log($"{playerOne.Name} ({playerOne.Cards.Count + playerOne.DiscardDeck.Count + winnerCards.Count / 2} cards): {cardFirstPlayer.Name} {cardFirstPlayer.Suit}\n");
                    _outputService.Log($"{playerTwo.Name} ({playerTwo.Cards.Count + playerTwo.DiscardDeck.Count + winnerCards.Count / 2} cards): {cardSecondPlayer.Name} {cardSecondPlayer.Suit}\n\n");
                    if (cardFirstPlayer > cardSecondPlayer)
                    {
                        _outputService.Log(OutputConstants.WinPlayerInRound, playerOne.Name);
                        winnerCards.Add(cardFirstPlayer);
                        winnerCards.Add(cardSecondPlayer);
                        playerOne.DiscardDeck.AddRange(winnerCards);
                        winnerCards = new List<Card>();
                    }
                    else if (cardFirstPlayer < cardSecondPlayer)
                    {
                        _outputService.Log(OutputConstants.WinPlayerInRound, playerTwo.Name);
                        winnerCards.Add(cardFirstPlayer);
                        winnerCards.Add(cardSecondPlayer);
                        playerTwo.DiscardDeck.AddRange(winnerCards);
                        winnerCards = new List<Card>();
                    }
                    else
                    {
                        _outputService.Log(OutputConstants.NoWin);
                        winnerCards.Add(cardFirstPlayer);
                        winnerCards.Add(cardSecondPlayer);
                    }

                    playerTwo.Cards.Remove(cardSecondPlayer);
                    playerOne.Cards.Remove(cardFirstPlayer);

                    if (!playerOne.Cards.Any())
                    {
                        playerOne.Cards = _cardService.ShuffleCards(playerOne.DiscardDeck);
                        playerOne.DiscardDeck = new List<Card>();
                    }
                    if (!playerTwo.Cards.Any())
                    {
                        playerTwo.Cards = _cardService.ShuffleCards(playerTwo.DiscardDeck);
                        playerTwo.DiscardDeck = new List<Card>();
                    }
                }

                return playerOne.Cards.Count > playerTwo.Cards.Count ? playerOne : playerTwo;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
