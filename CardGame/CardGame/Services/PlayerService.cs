﻿using CardGame.Interfaces;
using System.Collections.Generic;

namespace CardGame.Services
{
    public class PlayerService : IPlayerService
    {
        public List<Player> CreatePayers(int countOfPlayer)
        {
            var players = new List<Player>();

            for(var i = 1; i <= countOfPlayer; i++)
            {
                var player = new Player
                {
                    Name = $"Player{i}",
                };
                players.Add(player);
            }

            return players;
        }
    }
}
