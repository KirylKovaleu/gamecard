﻿using CardGame.Interfaces;
using System;

namespace CardGame.Services
{
    public class OutputService : IOutputService
    {
        public void Log<T>(string message, T info)
        {
            Console.WriteLine(message, info);
        }

        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
