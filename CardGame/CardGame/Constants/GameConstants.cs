﻿namespace CardGame.Constants
{
    public static class GameConstants
    {
        public const string WinPlayerInRound = "{0} wins this round \n\n";
        public const string NoWin = "No winner in this round \n\n";
        public const string WinPlayerInRGame = "{0} wins the game!";
        public const string NotCorrectNumber = "Not Correct Number";
    }
}
