﻿namespace CardGame
{
    public class Card
    {
        public int Number { get; set; }

        public string Name { get; set; }

        public string Suit { get; set; }

        public static bool operator >(Card c1, Card c2)
        {
            return c1.Number > c2.Number;
        }
        public static bool operator <(Card c1, Card c2)
        {
            return c1.Number < c2.Number;
        }
    }
}
