﻿using System.Collections.Generic;

namespace CardGame
{
    public class Player
    {
        public string Name { get; set; }

        public List<Card> Cards { get; set; }

        public List<Card> DiscardDeck { get; set; }

        public Player()
        {
            Cards = new List<Card>();
            DiscardDeck = new List<Card>();
        }
    }
}
